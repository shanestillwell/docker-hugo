> Create a docker container for Hugo

To deploy a new version, just create a git tag and push the tags.

For example

```
git tag 0.40.3
git push origin master --tags
```


The CI should pick up the new tag, build it and push it to Docker Hub


https://gohugo.io/news/

In my projects, I just have a `server.sh` file that contains

```
#!/bin/bash
VERSION=${1:-0.47}

docker run --rm -v `pwd`:/src -p=1313:1313 northernv/hugo:$VERSION hugo server --bind 0.0.0.0 -D
```

Then you can start the server locally with `./server.sh`
